#### Font Awesome
```mermaid
graph LR
   FONTAWESOME["fa:fa-gitlab"]
```

#### GitLab icons
```mermaid
graph LR
   GITLAB[":monkey:"]
```


#### UNICODE
```mermaid
graph LR
   UNICODE["📊"]
```

<h1> I'm big</h1>
<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> Orange GitLab Tanuki

<i class="fas fa-camera"></i> camera <!-- this icon's 1) style prefix == fas and 2) icon name == camera -->
📊♠❤
